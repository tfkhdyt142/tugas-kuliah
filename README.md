## Tugas Kuliah Pemrograman
<img src="https://i.postimg.cc/CLzRsx7Q/TKP-Banner.jpg" alt="banner">
<p align=center>
  <a href="https://facebook.com/tfkhdyt142"><img height="30" src="https://upload.wikimedia.org/wikipedia/commons/5/51/Facebook_f_logo_%282019%29.svg"></a>&nbsp;
  <a href="https://twitter.com/tfkhdyt"><img height="30" src="https://upload.wikimedia.org/wikipedia/en/6/60/Twitter_Logo_as_of_2021.svg"></a>&nbsp;
  <a href="https://instagram.com/_tfkhdyt_"><img height="30" src="https://upload.wikimedia.org/wikipedia/commons/e/e7/Instagram_logo_2016.svg"></a>&nbsp;
  <a href="https://youtube.com/tfkhdyt"><img height="30" src="https://upload.wikimedia.org/wikipedia/commons/a/a0/YouTube_social_red_circle_%282017%29.svg"></a>&nbsp;
  <a href="https://t.me/tfkhdyt"><img height="30" src="https://upload.wikimedia.org/wikipedia/commons/8/83/Telegram_2019_Logo.svg"></a>&nbsp;
  <a href="https://open.spotify.com/playlist/4JR5wqcnuOQw6ppF38Vpu9?si=zHMKBfCiRrGVamKsL8LXqQ"><img height="30" src="https://upload.wikimedia.org/wikipedia/commons/1/19/Spotify_logo_without_text.svg"></a>
  <a href="https://pddikti.kemdikbud.go.id/data_mahasiswa/QUUyNzdEMjktNDk0Ri00RTlDLUE4NzgtNkUwRDBDRjIxOUNB"><img height="30" src="https://i.postimg.cc/YSB2c3DG/1619598282440.png"></a>
  <a href="https://www.linkedin.com/mwlite/in/taufik-hidayat-6793aa200"><img height="30" src="https://upload.wikimedia.org/wikipedia/commons/8/81/LinkedIn_icon.svg"></a>
</p>
<p align=center>
  <table width="75%" align="center">
    <tr>
      <td colspan=2 align="center"><b>Data Diri</b></td>
    </tr>
    <tr>
      <td><b>Nama</b></td>
      <td>Taufik Hidayat</td>
    </tr>
    <tr>
      <td><b>NIM</b></td>
      <td>301200032</td>
    </tr>
    <tr>
      <td><b>Fakultas</b></td>
      <td>Teknologi Informasi</td>
    </tr>
    <tr>
      <td><b>Prodi</b></td>
      <td>Teknik Informatika</td>
    </tr>
    <tr>
      <td><b>Per. Tinggi</b></td>
      <td>Universitas Bale Bandung</td>
    </tr>
  </table>
</p>

### Daftar Isi
<a href="https://github.com/tfkhdyt/tugas-kuliah/archive/refs/heads/main.zip"><b>Download All</b></a>

- [Semester 1](https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%201) | [**Download**](https://minhaskamal.github.io/DownGit/#/home?url=https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%201)
  - [Algoritma & Pemrograman 1](https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%201/Algoritma%20%26%20Pemrograman%20(Pak%20Yudi%20%26%20Pak%20Cecep)) | [**Download**](https://minhaskamal.github.io/DownGit/#/home?url=https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%201/Algoritma%20%26%20Pemrograman%20(Pak%20Yudi%20%26%20Pak%20Cecep))
  - [Pengantar Java](https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%201/Pengantar%20Java%20(Pak%20Yusuf%20%26%20Pak%20Ridwan)) | [**Download**](https://minhaskamal.github.io/DownGit/#/home?url=https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%201/Pengantar%20Java%20(Pak%20Yusuf%20%26%20Pak%20Ridwan))
- [Semester 2](https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%202) | [**Download**](https://minhaskamal.github.io/DownGit/#/home?url=https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%202)
  - [Algoritma & Pemrograman 2](https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%202/Algoritma%20%26%20Pemrograman%202%20(Pak%20Yudi)) | [**Download**](https://minhaskamal.github.io/DownGit/#/home?url=https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%202/Algoritma%20%26%20Pemrograman%202%20(Pak%20Yudi))
  - [Pemrograman Java Lanjutan](https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%202/Pemrograman%20Java%20Lanjutan%20(Pak%20Yusuf)) | [**Download**](https://downgit.github.io/#/home?url=https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%202/Pemrograman%20Java%20Lanjutan%20(Pak%20Yusuf))
  - [Praktikum Pengantar Pemrograman](https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%202/Praktikum%20Pengantar%20Pemrograman%20(Pak%20Ojan)) | [**Download**](https://minhaskamal.github.io/DownGit/#/home?url=https://github.com/tfkhdyt/tugas-kuliah/tree/main/Semester%202/Praktikum%20Pengantar%20Pemrograman%20(Pak%20Ojan))
  
### Support
Klik tombol di bawah untuk mendukung saya lewat donasi

<p align="center">
  <a href="https://tfkhdyt.web.app/donate">
    <img src="https://i.postimg.cc/jjRDbZQx/1621036430601.png" width="40%">
  </a>
</p>
